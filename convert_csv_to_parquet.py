import argparse
import pandas as pd
from pathlib import Path
from tools import get_logger
from logging import Logger


def main(input_file_path: Path, logger: Logger):
    """Converts a given CSV file to parquet. This allows for faster processing in
    subsequent scripts.

    Args:
        input_file_path (Path): Input file path
        logger (Logger): Logger

    Raises:
        ValueError: If the input file does not end in .csv, a ValueError is raised.
    """
    base, extension = (input_file_path.with_suffix(""), input_file_path.suffix)
    if extension == ".csv":
        df = pd.read_csv(input_file_path)
        output_file_path = base.with_suffix(".parquet")
        df.to_parquet(output_file_path)
        logger.info(
            f"Converted CSV succesfully to parquet. File saved as {output_file_path}"
        )
    else:
        raise ValueError(
            "Input file is not a CSV file. Please use a csv file as input."
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Converts CSV files to parquet for faster processing."
    )
    parser.add_argument(
        "-i",
        "--input_file",
        required=True,
        help="Location of file to convert to parquet.",
    )
    options = parser.parse_args()
    logger = get_logger(f"{__file__}.log")
    main(Path(options.input_file), logger)
