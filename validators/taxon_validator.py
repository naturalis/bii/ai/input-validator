from collections import Counter, defaultdict
from typing import Set, List

import numpy as np
import pandas
from tqdm import tqdm

ranks = [
    "kingdom",
    "division",
    "class",
    "order",
    "family",
    "genus",
    "species",
    "infraspecies",
]

higher_ranks = [
    "kingdom",
    "division",
    "class",
    "order",
    "family",
    "genus",
]

rank_to_depth = dict(list(zip(ranks, list(range(len(ranks))))))
higher_taxa_sub = {}


def name_is_last(row_):
    taxonomy = [
        x_
        for x_ in row_[
            [
                "kingdom",
                "division",
                "class",
                "order",
                "family",
                "genus",
                "species",
                "infraspecies",
            ]
        ]
        if not pandas.isna(x_)
    ]
    # print(taxonomy)
    return (
        taxonomy[-1] == row_["taxon_full_name"]
        or row_["taxon_full_name"] in higher_taxa_sub
    )


def max_repeat(row_, max_rank=8):
    taxonomy = [
        x_
        for x_ in row_[
            [
                "kingdom",
                "division",
                "class",
                "order",
                "family",
                "genus",
                "species",
                "infraspecies",
            ][:max_rank]
        ]
        if not (pandas.isna(x_) or x_.strip() == "")
    ]
    if len(taxonomy) == 0:
        print(row_)
        raise RuntimeError("All the values in the taxonomy are empty")
    return int(Counter(taxonomy).most_common()[0][1])


def compute_rank(row_):
    taxonomy = np.array(
        [x_.split("$")[0] if not pandas.isna(x_) else x_ for x_ in row_[ranks[:5]]]
    )  # TODO: genus and deeper are not computed yet, check for other sources than wrn
    t_ = np.where(taxonomy == row_["taxon_full_name"])[0]
    # if row_["taxon_full_name"] == "Cladocera":
    #     print("oemba", t_, row_["taxon_full_name"].split("$")[0])
    # print(taxonomy)
    return (
        (None if len(t_) == 0 else ranks[t_[0]])
        if pandas.isna(row_["rank"])
        else row_["rank"]
    )


spec_to_remove = {"spec.", "indet.", "spec", "indet", "Indet."}


def compute_infra_species(taxa):
    taxa["species"] = taxa.apply(
        lambda row_: (
            row_["genus"] + " " + row_["specific_epithet"]
            if (
                not pandas.isna(row_["specific_epithet"])
                and not pandas.isna(row_["genus"])
            )
            else None
        ),
        axis=1,
    )
    taxa["infraspecies"] = taxa.apply(
        lambda row_: (
            row_["genus"]
            + " "
            + row_["specific_epithet"]
            + " "
            + row_["infraspecific_epithet"]
            if (
                not pandas.isna(row_["specific_epithet"])
                and not pandas.isna(row_["genus"])
                and not pandas.isna(row_["infraspecific_epithet"])
            )
            else None
        ),
        axis=1,
    )


def remove_spec(x_):
    result = []
    for token in x_.split():
        if token not in spec_to_remove:
            result.append(token)

    result = " ".join(result)
    return result if len(result.strip()) > 0 else None


def check_taxa(taxa) -> (pandas.DataFrame, Set[str]):
    taxa["taxon_full_name"] = taxa["taxon_full_name"].apply(lambda x_: remove_spec(x_))

    if "specific_epithet" not in taxa:
        taxa["specific_epithet"] = None
    if taxa.specific_epithet.isnull().all():
        # TODO(this is unlikely to happen in practice and likely indicates an invalid taxa file)
        taxa.specific_epithet = taxa.taxon_full_name.apply(
            lambda x_: x_.split()[1] if len(x_.split()) > 1 else None
        )
    else:
        taxa["specific_epithet"] = taxa["specific_epithet"].apply(
            lambda x_: remove_spec(x_) if not pandas.isna(x_) else x_
        )

    if "infraspecific_epithet" not in taxa:
        taxa["infraspecific_epithet"] = None
    if taxa.infraspecific_epithet.isnull().all():
        # TODO(this is unlikely to happen in practice and likely indicates an invalid taxa file)
        taxa.infraspecific_epithet = taxa.taxon_full_name.apply(
            lambda x_: " ".join(x_.split()[2:]) if len(x_.split()) > 2 else None
        )
    #
    compute_infra_species(taxa)

    for rank in ranks:
        if rank in taxa.columns:
            taxa.loc[taxa[rank] == "Not available", rank] = None

    # no repeats
    taxa["max_repeat"] = taxa.apply(lambda row_: max_repeat(row_), axis=1)
    taxa["not_name_is_last"] = taxa.apply(lambda row_: not name_is_last(row_), axis=1)
    taxa["no_ae_family"] = taxa["family"].apply(
        lambda x_: (
            (
                not x_.endswith("ae")
                and not x_.endswith("ea")
                and not x_.endswith("ae+")
                and not x_.endswith("incertae sedis")
                and x_ not in higher_taxa_sub
            )
            if not pandas.isna(x_)
            else False
        )
    )

    values_per_rank = defaultdict(lambda: set())
    for rank in ranks:
        values_per_rank[rank] = set(
            [x_ for x_ in taxa[rank].unique() if not pandas.isna(x_)]
        )

    indices_to_check = []
    for r, rank in enumerate(ranks):
        if r > 0:
            for rank2 in ranks[:r]:
                intersec = values_per_rank[rank].intersection(values_per_rank[rank2])
                if len(intersec) > 0:
                    for val in intersec:
                        taxa_problem = taxa[taxa[rank] == val]
                        indices_to_check.extend(taxa_problem.index.values.tolist())
    taxa["rank_repeats"] = False
    taxa.loc[indices_to_check, "rank_repeats"] = True
    # completeness
    # ranks = ["kingdom", "division", "class", "order", "family", "genus"]
    unique_higher_taxa = set(taxa[higher_ranks].values.flatten())
    missing_higher_taxa = unique_higher_taxa - set(taxa.taxon_full_name.unique()) - {None, np.nan}
    #
    return taxa[
        taxa.rank_repeats
        | taxa.no_ae_family
        | taxa.not_name_is_last
        | (taxa.max_repeat > 1)
    ], missing_higher_taxa


def reconstruct_taxon_name(
    rank_name: str, genus: str, specific_epithet: str, infraspecific_epithet: str
) -> str:
    """Reconstruct the full taxon name from the parts of the provided taxonomy.

    Args:
        rank_name (str): Name of the rank in `rank_value` of the taxonomy.
        genus (str): Name of the genus of the taxonomy.
        specific_epithet (str): Name of the specific epithet of the taxonomy.
        infraspecific_epithet (str): Name of the infraspecific epithet of the taxonomy.

    Returns:
        str: A reconstructed taxon name.
    """
    reconstructed_name = f"{genus} {specific_epithet}"
    if rank_name in ["subspecies", "infraspecific_epithet"]:
        reconstructed_name = f"{reconstructed_name} {infraspecific_epithet}"
    return reconstructed_name


def create_taxonomy_dict(taxonomy: List[str]) -> dict:
    """Create a dict from a list of values from a validation row. The dict's keys
    correspond to the columns that were combined to create `custom_taxonomy` in
    record_validator.validate_taxa.

    Args:
        taxonomy (List[str]): A list of headers that are combined into a list. This list
        should match the columns that were combined to create `custom_taxonomy` in
        record_validator.validate_taxa.

    Returns:
        dict: A dict with {headers: values} for a single row to be validated.
    """
    custom_taxonomy_headers = [
        "taxon_full_name",
        "rank_name",
        "rank_value",
        "kingdom",
        "division",
        "class",
        "order",
        "family",
        "genus",
        "specific_epithet",
        "infraspecific_epithet",
    ]
    return dict(zip(custom_taxonomy_headers, taxonomy))


def rank_value_equals_last_entry(taxonomy: List[str]) -> bool:
    """Checks is rank_value corresponds to the last non empty value in the taxonomy.

    Example:

    Using the following data:

    | # | ... | genus     | specific_epithet  | rank_name        | rank_value        |
    | - | --- | --------- | ----------------- | ---------------- | ----------------- |
    | 1 | ... | Graptemys | pseudogeographica | specific_epithet | pseudogeographica |
    | 2 | ... | Taraxacum |                   | genus            | Andryala          |

    Row 1: `rank_value` equals `pseudogeographica` -> Check if specific_epithet's column
    `pseudogeographica` matches `pseudogeographica`. Yes. -> Valid.

    Row 2: `rank_value` equals `Andryala` -> Check if specific_epithet's column ``
    matches `Andryala`. Is empty. -> Check next level. -> Check if genus's column
    `Taraxacum` matches `Andryala` No. -> Invalid.

    Args:
        taxonomy (List[str]): A list of headers that are combined into a list. This list
        should match the columns that were combined to create `custom_taxonomy` in
        record_validator.validate_taxa.

    Returns:
        bool: False if the validation finds errors, True otherwise.
    """
    taxonomy_dict = create_taxonomy_dict(taxonomy)
    last_non_empty_entry = ""
    for entry in reversed(list(taxonomy_dict.values())):
        if pandas.isna(entry) or entry == "*":
            continue
        else:
            last_non_empty_entry = entry
            continue

    if not last_non_empty_entry:
        return False
    if last_non_empty_entry != taxonomy_dict["rank_value"]:
        return False
    return True


def reconstructed_taxon_equals_taxon_full_name(taxonomy: List[str]) -> bool:
    """Check if the reconstructed taxon name equals the provided taxon_full_name.

    Example:

    Using the following data:

    | # | ... | taxon_full_name    | genus  | specific_epithet | infraspecific_epithet |
    | - | --- | ------------------ | ------ | ---------------- | --------------------- |
    | 1 | ... | Pieris rapae rapae | Pieris | rapae            | rapae                 |
    | 2 | ... | Pieris rapae       | Pieris | krueperi         |                       |

    Row 1: `taxon_full_name`: `Pieris rapae rapae` equals <genus> <specific_epithet>
    <infraspecific_epithet>: `Pieris rapae rapae`? Yes. -> Valid.

    Row 2: `taxon_full_name`: `Pieris rapae` equals <genus> <specific_epithet>
    <infraspecific_epithet>: `Pieris krueperi`? No. -> Invalid.

    Args:
        taxonomy (List[str]): A list of headers that are combined into a list. This list
        should match the columns that were combined to create `custom_taxonomy` in
        record_validator.validate_taxa.

    Returns:
        bool: False if the validation finds errors, True otherwise.
    """
    taxonomy_dict = create_taxonomy_dict(taxonomy)
    reconstructed_name = reconstruct_taxon_name(
        taxonomy_dict["rank_name"],
        taxonomy_dict["genus"],
        taxonomy_dict["specific_epithet"],
        taxonomy_dict["infraspecific_epithet"],
    )
    if taxonomy_dict["taxon_full_name"] != reconstructed_name:
        print(f"{taxonomy_dict['taxon_full_name']} != {reconstructed_name}")
        return False


def rank_value_equals_rank_name_column(taxonomy: List[str]) -> bool:
    """Validates if the rank_value provided matches the corresponding rank_name's
    column value.

    Example:

    Using the following data:

    | # | ... | genus     | specific_epithet  | rank_name        | rank_value        |
    | - | --- | --------- | ----------------- | ---------------- | ----------------- |
    | 1 | ... | Graptemys | pseudogeographica | specific_epithet | pseudogeographica |
    | 2 | ... | Taraxacum |                   | genus            | Andryala          |

    Row 1: rank_name equals specific_epithet -> Does specific_epithet's column:
    `pseudogeographica` match `rank_value`: `pseudogeographica`? Yes. -> Valid.

    Row 2: rank_name equals genus -> Does `genus`'s column: `Taraxacum` match
    `Andryala`? No. -> Invalid

    If rank_name does not match any known ranks, no validation can occur. Instead a
    warning is added to the warning file via a separate validation step.

    Args:
        taxonomy (List[str]): A list of headers that are combined into a list. This list
        should match the columns that were combined to create `custom_taxonomy` in
        record_validator.validate_taxa.

    Returns:
        bool: False if the validation finds errors, True otherwise.
    """
    taxonomy_dict = create_taxonomy_dict(taxonomy)
    rank_value = taxonomy_dict["rank_value"]
    rank_name = taxonomy_dict["rank_name"]
    if rank_name in ranks:
        rank_name = "specific_epithet" if rank_name == "species" else rank_name
        rank_name = "infraspecific_epithet" if rank_name == "subspecies" else rank_name
        if rank_value != taxonomy_dict[rank_name]:
            return False
    return True
