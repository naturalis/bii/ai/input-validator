"""
WrnValidator class
"""

import json
from multiprocessing import Process, Lock
from tempfile import TemporaryDirectory
from typing import List, Any, Dict, Optional
from logging import Logger

import numpy as np
import pandas
import pandas_schema
from pandas.core.dtypes.common import is_categorical_dtype, is_numeric_dtype
from pandas_schema import ValidationWarning
from tqdm import tqdm
from pathlib import Path


# Custom error function to have the validator print out the errors during execution
from reference_higher_taxa import complete_higher_taxa_file, make_higher_consistent
from validators.taxon_validator import (
    check_taxa,
    rank_value_equals_last_entry,
    rank_value_equals_rank_name_column,
    reconstructed_taxon_equals_taxon_full_name,
)

from pandas_schema.validation import (
    LeadingWhitespaceValidation,
    TrailingWhitespaceValidation,
    CustomSeriesValidation,
    CustomElementValidation,
    IsDistinctValidation,
    InRangeValidation,
    CanConvertValidation,
    DateFormatValidation,
    InListValidation,
)
from custom_validation.WarningValidation import WarningInListValidation


def my_get_errors(self, series: pandas.Series, column: pandas_schema.Column):
    errors = []
    # print("Validating column {}".format(column.name))
    # Calculate which columns are valid using the child class's validate function, skipping empty entries if the
    # column specifies to do so
    simple_validation = ~self.validate(series)
    if column.allow_empty:
        # Failing results are those that are not empty, and fail the validation
        # explicitly check to make sure the series isn't a category because issubdtype will FAIL if it is
        if is_categorical_dtype(series) or is_numeric_dtype(series):
            validated = ~series.isnull() & simple_validation
        else:
            validated = (series.str.len() > 0) & simple_validation

    else:
        validated = simple_validation

    # Cut down the original series to only ones that failed the validation
    indices = series.index[validated]

    # Use these indices to find the failing items. Also print the index which is probably a row number
    for i in indices:
        element = series[i]
        errors.append(
            ValidationWarning(
                message=self.message, value=element, row=i, column=series.name
            )
        )
        # print(
        #     "Error : {}, value : {}, row : {}, column : {}".format(
        #         self.message, element, i, series.name
        #     )
        # )

    return errors


class NaturalisAiInputValidator:
    """
    This class provides function to validate input data for Naturalis AI pipeline
    """

    def __init__(
        self,
        images_file: Path | None,
        taxa_file: Path | None,
        logger: Logger,
        max_threads: int = 0,
        check_unique_taxa: bool = False,
        out_file_csv: str = None,
        autocomplete_higher_taxa=False,
        make_higher_taxa_consistent=False,
    ):
        self.check_unique_taxa = check_unique_taxa
        self.lock = Lock()
        self.images_file = images_file
        self.taxa_file = taxa_file
        self.logger = logger
        self.chunk_size = 4096 * 8
        self.images_df = None
        self.taxa_df: Optional[pandas.DataFrame] = None
        self.validation_errors: Dict[str, List] = {"images": [], "taxa": []}
        self.max_threads: int = max_threads
        self.out_file_csv = out_file_csv

        self.known_ranks = [
            "kingdom",
            "division",
            "class",
            "order",
            "phylum",
            "family",
            "genus",
            "specific_epithet",
            "species",
            "infraspecific_epithet",
            "subspecies",
            "infraspecies",
        ]

        # Predefine the custom series validations
        self.custom_not_null = CustomSeriesValidation(
            lambda x: ~pandas.isnull(x), "Must not be empty"
        )
        self.custom_qualified_id = CustomElementValidation(
            lambda s: len(s.split(":")) == 2,
            "Is not a qualified ID of the form 'SRC:ID'",
        )
        self.custom_no_colon = CustomElementValidation(
            lambda s: pandas.isna(s) or ":" not in s, "Field should not contain ':'"
        )
        self.custom_qualified_id_or_empty = CustomElementValidation(
            self.is_qualified_id_or_empty_validation,
            "Is not a qualified ID of the form 'SRC:ID'",
        )
        self.custom_taxid_in_tax_file = CustomElementValidation(
            lambda s: self.taxon_id_in_taxa_file(s),
            "taxon_id_at_source not in taxa file",
        )
        self.custom_max_repeat = CustomElementValidation(
            lambda s: s == 1,
            "at least one of the rank value occurs more than once",
        )
        self.custom_rank_value_equals_last_entry = CustomElementValidation(
            rank_value_equals_last_entry,
            "Rank value does not equal the lowest rank of the taxonomy",
        )
        self.custom_rank_value_equals_rank_name_column = CustomElementValidation(
            rank_value_equals_rank_name_column,
            "rank_value does not equal the the value in the column of rank_name.",
        )
        self.custom_reconstructed_taxon_equals_taxon_full_name = CustomElementValidation(
            reconstructed_taxon_equals_taxon_full_name,
            "Taxon name reconstructed from taxonomy does not equal taxon_full_name. ",
        )

        # I can't see how to deal with optional columns in pandas_schema, so have to do it ourselves:
        self.mandatory_columns_images = [
            "image_url",
            "observation_id",
            "image_id",
            "taxon_id_at_source",
            "taxon_full_name",
        ]
        self.mandatory_columns_taxa = [
            "taxon_full_name",
            "rank_name",
            "rank_value",
            "taxon_id_at_source",
            "status_at_source",
            "accepted_taxon_id_at_source",
            "kingdom",
            "division",
            "class",
            "order",
            "family",
            "genus",
        ]

        CustomSeriesValidation.get_errors = my_get_errors
        CustomElementValidation.get_errors = my_get_errors
        LeadingWhitespaceValidation.get_errors = my_get_errors
        IsDistinctValidation.get_errors = my_get_errors
        InRangeValidation.get_errors = my_get_errors
        CanConvertValidation.get_errors = my_get_errors
        DateFormatValidation.get_errors = my_get_errors
        InListValidation.get_errors = my_get_errors
        WarningInListValidation.get_errors = my_get_errors

        if self.images_file:
            self.logger.info(f"Loading images file {self.images_file}")
            if self.images_file.suffix in [".pq", ".pqt", ".parquet"]:
                self.images_df = pandas.read_parquet(self.images_file)
            elif self.images_file.suffix in [".csv"]:
                self.images_df = pandas.read_csv(
                    self.images_file, dtype={"image_id": str, "observation_id": str}
                )
            else:
                raise ValueError(
                    "Extensions other than [preferred] parquet (.pq, .pqt or .parquet) "
                    "and CSV (.csv) are not supported. Please use the correct file "
                    "format."
                )

            for col in self.mandatory_columns_images:
                if col not in self.images_df.columns:
                    self.logger.error(
                        "Column {} is missing in images file - aborting".format(col)
                    )
                    exit(1)
        if self.taxa_file:
            if autocomplete_higher_taxa:
                self.logger.info("Auto-completing higher taxa")
                complete_higher_taxa_file(
                    self.taxa_file, self.taxa_file + ".completed.csv", logger=logger
                )
                self.taxa_file = self.taxa_file + ".completed.csv"
                self.logger.info(
                    "Auto-completing done. It's your OWN responsibility to validate the results"
                )
            if make_higher_taxa_consistent:
                self.logger.info("Making higher taxa consistent")
                taxa = pandas.read_csv(self.taxa_file)
                taxa = make_higher_consistent(
                    taxa, mode="strict", do_not_use_gbif_for_first=True, logger=logger
                )
                self.taxa_file = self.taxa_file + ".consistent.csv"
                taxa.to_csv(self.taxa_file, index=False)
                self.logger.info(
                    "Making consistent done. It's your OWN responsibility to validate the results"
                )

            self.logger.info(f"Loading taxa file {self.taxa_file}")
            if self.taxa_file.suffix in [".pq", ".pqt", ".parquet"]:
                self.taxa_df = pandas.read_parquet(self.taxa_file)
            elif self.taxa_file.suffix in [".csv"]:
                self.taxa_df = pandas.read_csv(
                    self.taxa_file, dtype={"image_id": str, "observation_id": str}
                )
            else:
                raise ValueError(
                    "Extensions other than [preferred] parquet (.pq, .pqt or .parquet) "
                    "and CSV (.csv) are not supported. Please use the correct file "
                    "format."
                )
            if "rank_value" not in self.taxa_df:
                self.taxa_df["rank_value"] = None
                self.taxa_df["rank_name"] = None
                self.taxa_df["genus"] = None
            for col in self.mandatory_columns_taxa:
                if col not in self.taxa_df.columns:
                    self.logger.error(
                        "Column {} is missing in taxa file - aborting".format(col)
                    )
                    exit(1)
            self.unique_taxa = set(self.taxa_df["taxon_id_at_source"])

    @staticmethod
    def is_qualified_id_or_empty_validation(value_: Any):
        string_value = str(value_)
        result = string_value == "nan" or len(string_value.split(":")) == 2
        return result

    def taxon_id_in_taxa_file(self, taxon_id: str):
        result = True
        if self.taxa_df is not None and not pandas.isnull(taxon_id):
            result = taxon_id in self.unique_taxa
        return result

    def validate(self):
        if self.images_df is not None:
            self.validation_errors["images"] = self.validate_images()
        if self.taxa_df is not None:
            errors = self.validate_taxa()
            self.validation_errors["taxa"] = errors

    def validate_images(self):

        schema = pandas_schema.Schema(
            [
                # image_url:
                pandas_schema.Column(
                    "image_url",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        IsDistinctValidation(),
                        self.custom_not_null,
                    ],
                ),
                # observation_id:
                pandas_schema.Column(
                    "observation_id",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_qualified_id,
                        self.custom_not_null,
                    ],
                ),
                # image_id:
                pandas_schema.Column(
                    "image_id",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        IsDistinctValidation(),
                        self.custom_qualified_id,
                        self.custom_not_null,
                    ],
                ),
                # taxon_id_at_source
                pandas_schema.Column(
                    "taxon_id_at_source",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_qualified_id,
                        self.custom_taxid_in_tax_file,
                        self.custom_not_null,
                    ],
                ),
                # taxon_full_name
                pandas_schema.Column(
                    "taxon_full_name",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_not_null,
                        self.custom_no_colon,
                    ],
                ),
                # sex
                pandas_schema.Column(
                    "sex",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_no_colon,
                    ],
                    allow_empty=True,
                ),
                # morph
                pandas_schema.Column(
                    "morph",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_no_colon,
                    ],
                    allow_empty=True,
                ),
                # morph_id
                pandas_schema.Column(
                    "morph_id",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_qualified_id,
                    ],
                    allow_empty=True,
                ),
                # location_latitude
                pandas_schema.Column(
                    "location_latitude",
                    [InRangeValidation(-90, 90), CanConvertValidation(float)],
                    allow_empty=True,
                ),
                # location_longitude
                pandas_schema.Column(
                    "location_longitude",
                    [
                        InRangeValidation(-180, 180),
                        CanConvertValidation(float),
                    ],
                    allow_empty=True,
                ),
                # rijkdriehoeksstelsel_x
                pandas_schema.Column(
                    "rijkdriehoeksstelsel_x",
                    [CanConvertValidation(float)],
                    allow_empty=True,
                ),
                # rijkdriehoeksstelsel_y
                pandas_schema.Column(
                    "rijkdriehoeksstelsel_y",
                    [CanConvertValidation(float)],
                    allow_empty=True,
                ),
                # date
                pandas_schema.Column(
                    "date",
                    [DateFormatValidation("%Y-%m-%d")],
                    allow_empty=True,
                ),
                # time
                pandas_schema.Column(
                    "time", [DateFormatValidation("%H:%M")], allow_empty=True
                ),
            ]
        )

        self.logger.info(
            "Validating {} rows in images file".format(len(self.images_df))
        )
        cols = [c for c in self.images_df.columns if c in schema.get_column_names()]

        num_chunks = int(np.ceil(len(self.images_df) / self.chunk_size))
        if num_chunks > 1:
            self.logger.info(
                "Validating data in {} chunks of size {}".format(
                    num_chunks, self.chunk_size
                )
            )

        if num_chunks == 1:
            self.max_threads = 0

        errors = []
        self.logger.info("")
        if self.max_threads > 0:
            threads = []
            with TemporaryDirectory() as temp_dir:
                self.logger.info(f"Writing JSON to temp directory {temp_dir}")
                cull_threads = max(1, int(0.5 * self.max_threads))
                for c, chunk in tqdm(
                    enumerate(np.array_split(self.images_df, num_chunks)),
                    total=num_chunks,
                ):
                    thread = Process(
                        target=self.worker,
                        args=(chunk, schema, cols, f"{temp_dir}/{c}.json"),
                    )
                    threads.append(thread)
                    thread.start()

                    # if the pool is filled, complete half of the threads before adding new ones
                    if len(threads) >= self.max_threads:
                        for thread_ in threads[:cull_threads]:
                            thread_.join()
                        threads = threads[cull_threads:]

                for thread_ in threads:
                    thread_.join()

                for chunk_num in range(num_chunks):
                    chunk_path = f"{temp_dir}/{chunk_num}.json"
                    errors.extend(json.load(open(chunk_path))["items"])

        else:
            for chunk in tqdm(np.array_split(self.images_df, num_chunks)):
                errors.extend(schema.validate(chunk, columns=cols))
        self.logger.info(f"Found {len(errors)} validation errors")

        return errors

    @staticmethod
    def worker(
        chunk: pandas.DataFrame, schema, columns: List[str], chunk_out_path: str
    ):

        errors_chunk = schema.validate(chunk, columns=columns)
        # use the disk as shared memory when multiprocessing
        with open(chunk_out_path, "w") as f:
            json.dump({"items": [e.__dict__ for e in errors_chunk]}, f)

    def validate_taxa(self):
        """
        Validate dataframe with taxon entries
        :return: validation errors
        :rtype: dict
        """
        error_taxa, missing_taxa = check_taxa(self.taxa_df)
        errors: List[ValidationWarning | dict] = []
        for r_i, row in error_taxa.iterrows():
            for error_column in [
                "no_ae_family",
                "not_name_is_last",
                # "rank_repeats" # TODO(fix first)
            ]:
                if row[error_column]:
                    errors.append(
                        {
                            "message": {
                                "rank_repeats": "at least one of the rank values in another rank",
                                "no_ae_family": "taxon is of rank family but does not end with ae",
                                "not_name_is_last": "the full taxon name cannot be reconstructed from the taxonomy",
                            }[error_column],
                            "value": 1,
                            "column": error_column,
                            "row": r_i,
                        }
                    )
        for taxon in missing_taxa:
            errors.append(
                {
                    "message": "higher_taxon_missing",
                    "value": taxon,
                    "column": "taxon_full_name",
                    "row": -1,
                }
            )

        #
        # Add wildcards: If kingdom is "Plantae", "Fungi" or "Protozoa",
        # we do not need the levels division, class, order
        for r in ["division", "class", "order"]:
            self.taxa_df.loc[self.taxa_df.kingdom == "Plantae", r] = "*"
            self.taxa_df.loc[self.taxa_df.kingdom == "Fungi", r] = "*"
            self.taxa_df.loc[self.taxa_df.kingdom == "Protozoa", r] = "*"

        # Insert column with combination: taxon_full_name, family
        # this is to test for cases with same taxon name referring to the same concept
        if not self.check_unique_taxa:
            self.taxa_df.insert(
                loc=0,
                column="taxon_full_name_+_family",
                value=list(zip(self.taxa_df.taxon_full_name, self.taxa_df.family)),
            )

        # pandas_schema does not natively allow for checking values across a row. A
        # workaround is to combine existing columns into one column to allow for
        # checking the entire taxonomy structure.
        self.taxa_df.insert(
            loc=0,
            column="custom_taxonomy",
            value=list(
                zip(
                    self.taxa_df["taxon_full_name"],
                    self.taxa_df["rank_name"],
                    self.taxa_df["rank_value"],
                    self.taxa_df["kingdom"],
                    self.taxa_df["division"],
                    self.taxa_df["class"],
                    self.taxa_df["order"],
                    self.taxa_df["family"],
                    self.taxa_df["genus"],
                    self.taxa_df["specific_epithet"],
                    self.taxa_df["infraspecific_epithet"],
                )
            ),
        )
        self.taxa_df.insert(
            loc=0,
            column="kingdom_+_taxon_full_name",
            value=list(
                zip(
                    self.taxa_df["kingdom"],
                    self.taxa_df["taxon_full_name"],
                )
            ),
        )

        # validate taxa data frame
        self.make_taxon_schema()

        self.logger.info("Validating {} rows in taxa file".format(len(self.taxa_df)))
        cols = [
            c for c in self.taxa_df.columns if c in self.taxon_schema.get_column_names()
        ]
        errors.extend(self.taxon_schema.validate(self.taxa_df, columns=cols))

        self.logger.info("Found {} validation errors".format(len(errors)))

        if self.out_file_csv is not None:
            # TODO: add rows from pandas schema validation
            error_taxa.to_csv(self.out_file_csv, index=True)

        return errors

    def make_taxon_schema(self):
        self.taxon_schema = pandas_schema.Schema(
            [
                pandas_schema.Column(
                    "taxon_full_name",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_not_null,
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "taxon_id_at_source",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        IsDistinctValidation(),
                        self.custom_qualified_id,
                        self.custom_not_null,
                    ],
                ),
                pandas_schema.Column(
                    "status_at_source",
                    [
                        InListValidation(["accepted", "synonym"]),
                        self.custom_not_null,
                    ],
                ),
                pandas_schema.Column(
                    "accepted_taxon_id_at_source",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_qualified_id_or_empty,
                        self.custom_taxid_in_tax_file,
                    ],
                ),
                pandas_schema.Column(
                    "kingdom",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_not_null,
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "division",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_not_null,
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "class",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_not_null,
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "order",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_not_null,
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "family",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "genus",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "specific_epithet",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "infraspecific_epithet",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_no_colon,
                    ],
                ),
                pandas_schema.Column(
                    "rank_name",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        WarningInListValidation(
                            options=self.known_ranks, is_warning=True
                        ),
                    ],
                ),
                pandas_schema.Column(
                    "rank_value",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                    ],
                ),
                (
                    pandas_schema.Column(
                        "taxon_full_name_+_family",
                        [
                            IsDistinctValidation(),
                        ],
                    )
                    if not self.check_unique_taxa
                    else pandas_schema.Column(
                        "taxon_full_name",
                        [
                            IsDistinctValidation(),
                        ],
                    )
                ),
                pandas_schema.Column(
                    "custom_taxonomy",
                    [
                        LeadingWhitespaceValidation(),
                        TrailingWhitespaceValidation(),
                        self.custom_rank_value_equals_last_entry,
                        self.custom_rank_value_equals_rank_name_column,
                    ],
                ),
                pandas_schema.Column(
                    "max_repeat",
                    [
                        self.custom_max_repeat,
                    ],
                ),
                pandas_schema.Column(
                    "kingdom_+_taxon_full_name",
                    [
                        IsDistinctValidation(
                            message="Within kingdom:{value[0]}, {value[1]} is "
                            "not unique."
                        ),
                    ],
                ),
            ]
        )

    def log_errors(self, ftype: str):
        errors = self.validation_errors[ftype]
        if len(errors) == 0:
            self.logger.info("{} file valid".format(ftype))
        else:
            self.logger.info("{} file not valid".format(ftype))
            for error in errors:
                self.logger.info(
                    "Problem: {} row: {}, column: {}, value: {}".format(
                        error.message, error.row, error.column, error.value
                    )
                )

    def errors_to_file(self, outfile):
        # write json errors to file
        errors = {}
        if self.images_file:
            errors[self.images_file] = [
                e.__dict__ if hasattr(e, "__dict__") else e
                for e in self.validation_errors["images"]
            ]
        if self.taxa_file:
            errors[self.taxa_file] = [
                e.__dict__ for e in self.validation_errors["taxa"]
            ]

        with open(outfile, "w") as fp:
            fp.write(json.dumps(errors, indent=4))
