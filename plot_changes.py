import pandas
import matplotlib.pyplot as plt
import matplotlib
# matplotlib.use('TkAgg')
def m():
    t = pandas.read_csv("counts_merged.csv")

    t = t[(t.counts_prev>0)&(t.counts_new>0)]
    print(len(t))

    t["perc_change"] = t.apply(lambda row_: (row_["counts_new"]-row_["counts_prev"])/row_["counts_prev"], axis=1)
    t.sort_values("counts_prev", inplace=True)
    plt.scatter(t["counts_prev"], t.perc_change)
    plt.ylim(0,1)
    plt.savefig("bla.png")


if __name__ == '__main__':
    m()