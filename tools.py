import logging


def get_logger(filename="validator.log"):
    """
    Get a root logger on INFO level
    :return: logging.RootLogger
    """
    logger = logging.getLogger("")
    logger.setLevel("INFO")
    logger.handlers = []
    formatter = logging.Formatter("%(asctime)s %(message)s")

    console = logging.StreamHandler()
    console.setFormatter(formatter)
    console.setLevel("INFO")

    logfile = logging.FileHandler(filename)
    logfile.setFormatter(formatter)
    logfile.setLevel("INFO")

    logger.addHandler(console)
    logger.addHandler(logfile)
    return logger
