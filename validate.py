"""
Script to validate observation data as input for Naturalis AI model training pipeline
"""

import argparse
import time
from pathlib import Path

from compact_errors import compact_errors_memory
from tools import get_logger
from validators.record_validator import NaturalisAiInputValidator

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Validates Naturalis AI pipeline images and/or taxa file for model "
        "building"
    )
    parser.set_defaults(func=lambda x: parser.print_usage())
    parser.add_argument(
        "-i", "--images_file", required=False, help="Location of images file"
    )
    parser.add_argument(
        "-t", "--taxa_file", required=False, help="Location of taxa file"
    )
    parser.add_argument(
        "--number_of_threads",
        default=0,
        help="Number of threads to process data. Note that "
        "using > 1 thread can use a lot of memory. For example"
        " using 4 threads uses about 40 GiB for a 40M images file.",
        type=int,
    )
    parser.add_argument(
        "-o",
        "--out_file",
        required=False,
        help="Outfile with validation errors, defaults to, 'Y-m-d-H-M-S.json'",
        default=time.strftime("%Y-%m-%d-%H-%M-%S") + ".json",
    )
    parser.add_argument(
        "--check_unique_taxa",
        default=False,
        action="store_true",
        help="If True checks that taxon names are unique, not only the combination of "
        "(taxon name and family) or (taxon name and kingdom).",
    )
    parser.add_argument(
        "--autocomplete_higher_taxa",
        default=False,
        action="store_true",
        help="TODO: experimental",
    )
    parser.add_argument(
        "--make_higher_taxa_consistent",
        default=False,
        action="store_true",
        help="TODO: experimental",
    )
    parser.add_argument(
        "--out_file_csv",
        required=False,
        help="TODO",
        default=None,
    )
    options = parser.parse_args()
    logger = get_logger(options.out_file.replace(".json", ".log"))

    validator = NaturalisAiInputValidator(
        images_file=Path(options.images_file) if options.images_file else None,
        taxa_file=Path(options.taxa_file) if options.taxa_file else None,
        logger=logger,
        max_threads=options.number_of_threads,
        check_unique_taxa=options.check_unique_taxa,
        out_file_csv=options.out_file_csv,
        autocomplete_higher_taxa=options.autocomplete_higher_taxa,
        make_higher_taxa_consistent=options.make_higher_taxa_consistent,
    )
    validator.validate()
    compact_errors_memory(validator.validation_errors, options.out_file, logger=logger)
