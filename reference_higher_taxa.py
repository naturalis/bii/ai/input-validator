from typing import Optional

import logging

import hashlib

import pandas
from tqdm import tqdm
from unidecode import unidecode

# TODO: remove this when taxon map is on pypi
taxonmap_found = False
try:
    import src as taxonmap
    taxonmap_found = True
except ModuleNotFoundError:
    print("taxonmap not found, please clone from https://gitlab.com/naturalis/bii/ai/taxonmap and "
                       "add to PYTHONPATH environment variable")
import numpy as np

from validators.taxon_validator import compute_infra_species, higher_ranks


def get_best_taxa(
    ranks_concerned, taxa_sel, check_first_nan=True, do_not_use_gbif_for_first=True
):
    min_num_nan = len(ranks_concerned)
    i2_best = None
    for i2 in range(len(taxa_sel)):
        taxonomy_to_use = taxa_sel[ranks_concerned].values[i2]
        # print(taxonomy_to_use)
        num_nan = sum(
            [int(pandas.isna(x_)) for x_ in taxa_sel[ranks_concerned].values[i2]]
        )

        if (not pandas.isna(taxonomy_to_use[0]) or not check_first_nan) and (
            "GBIF" not in taxa_sel.taxon_id_at_source.values[i2]
            or not do_not_use_gbif_for_first
        ):
            if num_nan <= min_num_nan:
                min_num_nan = num_nan
                i2_best = i2
    return i2_best


def make_higher_consistent(
    taxa: pandas.DataFrame,
    mode="quirks",
    do_not_use_gbif_for_first=False,
    logger: Optional[logging.Logger] = logging.getLogger("make_higher_consistent"),
):
    global ranks
    ranks = ["genus", "family", "order", "class", "division", "kingdom"]
    for r, rank in enumerate(ranks):
        for rank_value in tqdm(taxa[rank].unique(), desc=f"consistify {rank}"):
            if rank_value is None or (
                isinstance(rank_value, float) and np.isnan(rank_value)
            ):
                continue
            taxa_sel = taxa[taxa[rank] == rank_value]
            if rank == "genus" and len(taxa_sel.kingdom.unique()) > 1:
                logging.info(f"skipping genus {rank_value} because multiple kingdom")
                continue
            if (
                do_not_use_gbif_for_first
                and "GBIF" in taxa_sel.taxon_id_at_source.values[0]
                and not all("GBIF" in x_ for x_ in taxa_sel.taxon_id_at_source)
            ):
                taxa_sel.to_csv("make_higher_consistent_error.csv", index=False)
                raise RuntimeError(f"First taxon is GBIF taxon {taxa_sel}")
            if len(taxa_sel) == 0:
                if mode == "quirks":
                    print(taxa_sel, rank_value)
                else:
                    raise RuntimeError(
                        f"No taxa found for {taxa[rank]} == {rank_value}"
                    )
            elif len(taxa_sel) == 1:
                continue
            ranks_concerned = ranks[r + 1 :]
            taxonomy_to_use = taxa_sel[ranks_concerned].values[0]

            if rank != "kingdom":
                # find best starting point according to rule
                i2_best = get_best_taxa(
                    ranks_concerned,
                    taxa_sel,
                    do_not_use_gbif_for_first=do_not_use_gbif_for_first,
                )
                if i2_best is not None:
                    taxonomy_to_use = taxa_sel[ranks_concerned].values[i2_best]
                else:
                    i2_best = get_best_taxa(
                        ranks_concerned,
                        taxa_sel,
                        check_first_nan=False,
                        do_not_use_gbif_for_first=do_not_use_gbif_for_first,
                    )
                    if i2_best is not None:
                        taxonomy_to_use = taxa_sel[ranks_concerned].values[i2_best]
                    else:
                        i2_best = get_best_taxa(
                            ranks_concerned,
                            taxa_sel,
                            check_first_nan=False,
                            do_not_use_gbif_for_first=False,
                        )
                        if i2_best is not None:
                            taxonomy_to_use = taxa_sel[ranks_concerned].values[i2_best]
                        else:
                            taxa_sel.to_csv(
                                "make_higher_consistent_error.csv", index=False
                            )
                            raise RuntimeError("bla")
                # then complete with other which is consistent
                not_na_idx = np.where(~pandas.isna(taxonomy_to_use))[0]
                na_idx = np.where(pandas.isna(taxonomy_to_use))[0]
                optimal_num_na_left = len(na_idx)
                best_completion_idx = None
                if len(na_idx) > 0:
                    for i2 in range(len(taxa_sel)):
                        if i2 == i2_best:
                            continue
                        other = taxa_sel[ranks_concerned].values[i2]
                        if not np.all(taxonomy_to_use[not_na_idx] == other[not_na_idx]):
                            # inconsistent
                            continue
                        else:
                            taxonomy_to_use2 = taxonomy_to_use
                            taxonomy_to_use2[na_idx] = other[na_idx]
                            num_na_left = len(
                                np.where(pandas.isna(taxonomy_to_use2))[0]
                            )
                            if num_na_left < optimal_num_na_left:
                                best_completion_idx = i2
                                optimal_num_na_left = num_na_left
                    if best_completion_idx is not None:
                        taxonomy_to_use[na_idx] = taxa_sel[ranks_concerned].values[
                            best_completion_idx
                        ][na_idx]

            # print(rank_value, first)
            for idx, _ in taxa_sel.iterrows():
                current = [
                    x_ for x_ in taxa.loc[idx, ranks_concerned] if not pandas.isna(x_)
                ]
                # print(taxonomy_to_use)

                target = [x_ for x_ in taxonomy_to_use if not pandas.isna(x_)]
                if not np.all(current == target):
                    logger.info(
                        f"changing {taxa.loc[idx, ranks_concerned].values} to {taxonomy_to_use}"
                    )
                    taxa.loc[idx, ranks_concerned] = taxonomy_to_use
    return taxa


def string_hash(s: str):
    """
    Computes a hash from a string.
    :param s:
    :return:
    """
    hasher = hashlib.sha224()
    hasher.update(s.encode())
    return hasher.hexdigest()


def complete_higher_taxa_file(
    in_path: str,
    out_path: str,
    logger: Optional[logging.Logger] = logging.getLogger("complete_higher_taxa_file"),
):
    t = pandas.read_csv(in_path)

    num_taxa = 0
    while True:
        t = complete_higher_taxa(t, out_path)
        if len(t) == num_taxa:
            break
        num_taxa = len(t)
        print(f"pass {len(t)}")

    t.to_csv(out_path, index=False)


def complete_higher_taxa(t: pandas.DataFrame, out_path):
    known_names = set(t.taxon_full_name)
    ranks = list(
        reversed(
            [
                "kingdom",
                "division",
                "class",
                "order",
                "family",
                "genus",
                "species",
                "infraspecies",
            ]
        )
    )

    # TODO(hierarchy): use ancestor to get correct taxon from GBIF
    unknown_names = set()
    map_rank = {"phylum": "division"}
    headers = [
        "taxon_id_at_source",
        "taxon_full_name",
        "status_at_source",
        "kingdom",
        "division",
        "class",
        "order",
        "family",
        "genus",
        "specific_epithet",
    ]
    if "infraspecies" not in t.columns or "species" not in t.columns:
        compute_infra_species(t)
    for column in ranks + ["taxon_full_name"]:
        t[column] = t[column].apply(lambda x_: unidecode(x_) if not pandas.isna(x_) else None)
    known_names = known_names.union(set(t.taxon_full_name))
    new_higher_taxa = []
    added_ids = set()
    added_names = set()
    for _, row in tqdm(t.iterrows(), total=len(t)):
        # is_higher_rank = False
        for r_i, rank in enumerate(ranks):
            rank_value = row[rank]
            is_higher_rank = rank in higher_ranks

            if (
                    is_higher_rank
                    and rank_value not in known_names
                    and not pandas.isna(rank_value)
                    and rank_value not in unknown_names
            ):
                rank_value = sanitize(rank_value)
                unknown_names.add(rank_value)
                if rank != "kingdom":
                    parent_rank = ranks[r_i + 1]
                    if not pandas.isna(row[parent_rank]):
                        parent_tuple = (parent_rank, row[parent_rank])
                    else:
                        parent_tuple = None
                    # print(rank_value, parent_tuple)
                    j_taxon = taxonmap.tools.io.gbif_search_taxon(
                        row["kingdom"], rank_value, True, parent_tuple=parent_tuple
                    )
                else:
                    j_taxon = taxonmap.tools.io.gbif_search_taxon(
                        row["kingdom"], rank_value, True
                    )
                if "rank" in j_taxon:
                    gbif_rank, is_match = check_match(
                        j_taxon, map_rank, parent_tuple, rank, rank_value, row
                    )

                    if not is_match:
                        if "alternatives" in j_taxon:
                            for j_taxon in j_taxon["alternatives"]:
                                gbif_rank, is_match = check_match(
                                    j_taxon,
                                    map_rank,
                                    parent_tuple,
                                    rank,
                                    rank_value,
                                    row,
                                )
                                if is_match:
                                    break

                    if is_match:
                        new_row = row_from_gbif_taxon(gbif_rank, j_taxon, rank_value, ranks)
                    else:  # not is_match
                        new_row = make_nia_row(rank_value, ranks, row)
                    if new_row[0] in added_ids:
                        continue
                    if new_row[3] + new_row[1] in added_names:
                        continue
                    new_row.extend(
                        [
                            None,
                        ]
                        * (len(headers) - len(new_row))
                    )
                    new_higher_taxa.append(new_row)
                    added_ids.add(new_row[0])
                    added_names.add(new_row[3] + new_row[1])
                    # print("new_row", new_row)

                else:
                    print("NO RANK IN RESULT", rank, rank_value, j_taxon)

    new_taxa_table = pandas.DataFrame(data=new_higher_taxa, columns=headers)
    all_taxa = pandas.concat([t, new_taxa_table], ignore_index=True)
    all_taxa.reset_index(drop=True, inplace=True)
    return all_taxa


def make_nia_row(rank_value, ranks, row):
    new_row = [
        f"NIA:{string_hash(rank_value)}",
        rank_value,
        "accepted",
    ]
    for rank_ in reversed(ranks):  # kingdom->infraspecies
        if pandas.isna(row[rank_]):
            new_row.append(None)
            continue
        rank_value2 = sanitize(row[rank_])
        new_row.append(unidecode(rank_value2))
        if rank_value2 == unidecode(rank_value):
            break
    return new_row


def row_from_gbif_taxon(gbif_rank, j_taxon, rank_value, ranks):
    # taxon_id_at_source	taxon_full_name	status_at_source	kingdom	division	class	order	family	genus	specific_epithet
    new_row = [
        f"GBIF:{j_taxon['usageKey']}",
        rank_value,
        "accepted",
    ]
    for rank_ in reversed(ranks):  # kingdom->infraspecies
        gbif_json_rank = rank_.replace("division", "phylum")
        gbif_rank_value = (
            j_taxon[gbif_json_rank]
            if gbif_json_rank in j_taxon
            else None
        )
        new_row.append(unidecode(gbif_rank_value) if gbif_rank_value else None)
        if rank_ == gbif_rank:
            break
    return new_row


def check_match(j_taxon, map_rank, parent_tuple, rank, rank_value, row):
    gbif_rank = j_taxon["rank"].lower()
    gbif_rank = map_rank.get(gbif_rank, gbif_rank)
    gbif_name = j_taxon["canonicalName"]
    is_match = False
    if gbif_rank == rank and gbif_name == rank_value:
        is_match = True
    else:
        if (
            ", genera incertae sedis" in rank_value
            or ", genera incertae sedis" in rank_value
            or ", families incertae sedis" in rank_value
        ):
            pass
            # print("KNOWN NOT FULL MATCH", gbif_rank, rank, gbif_name, rank_value)
        else:
            try:
                taxon_type = taxonmap.types.get_type(taxonmap.tools.taxa.QualifiedName(rank_value, "", "")).full_type
            except:
                print(f"error in getting type for {rank_value}")
            gbif_taxon_type = taxonmap.types.get_type(taxonmap.tools.taxa.QualifiedName(gbif_name, "", "")).full_type

            if taxon_type != gbif_taxon_type:
                print(
                    f"TYPES DO NOT MATCH '{rank_value}'={taxon_type} != '{gbif_name}'={gbif_taxon_type}"
                )
                print(
                    f"NO RANK MATCH, gbif_rank={gbif_rank}, rank={rank}, gbif_name={gbif_name}, rank_value={rank_value}, parent_tuple={parent_tuple}"
                )
                print(j_taxon)
            else:
                if gbif_name == rank_value and (gbif_rank != rank):
                    print(
                        f"NO RANK MATCH, gbif_rank={gbif_rank}, rank={rank}, gbif_name={gbif_name}, rank_value={rank_value}, parent_tuple={parent_tuple}"
                    )
                    print(j_taxon)
                elif gbif_rank == rank and (gbif_name != rank_value):
                    print(
                        f"NO NAME MATCH, gbif_rank={gbif_rank}, rank={rank}, gbif_name={gbif_name}, rank_value={rank_value}, parent_tuple={parent_tuple}"
                    )
                    print(j_taxon)
                    print(row)
                else:
                    print(
                        f"NO NAME & RANK MATCH, gbif_rank={gbif_rank}, rank={rank}, gbif_name={gbif_name}, rank_value={rank_value}, parent_tuple={parent_tuple}"
                    )
                    print(j_taxon)
    return gbif_rank, is_match


def sanitize(rank_value):
    if "$" in rank_value:
        rank_value = rank_value.split("$")[0].strip()
    if "(" in rank_value:
        rank_value = rank_value.split("(")[0].strip()
    rank_value = unidecode(rank_value)
    if "×" in rank_value:
        tokens = rank_value.split()
        if len(tokens) == 2 and tokens[1] == "×":
            rank_value = tokens[0]
    if rank_value.endswith(","):
        rank_value = rank_value[:-1]
    return rank_value


if __name__ == "__main__":
    # TODO: use argparse
    complete_higher_taxa_file(
        "/storage/msm24/model_package_repo/taxa_adb.csv",
        "/storage/msm24/model_package_repo/taxa_adb_higher.csv",
    )
