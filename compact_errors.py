import json
import argparse
import time
from collections import defaultdict
from typing import List, Dict
import logging

from tools import get_logger


def convert_list_to_runs(row_number_list: List[int]) -> List[str]:
    """Takes a list of numbers and converts for example [1, 2, 3] to [1-3] for easier readability.

    Args:
        row_number_list (list): A list of strings that represent integers.

    Returns:
        list: a list runs. For example: ["1", "3-4", "6", "8-15"]
    """
    runs = []
    # Separates out consecutive values in list of lists
    for row_number in row_number_list:
        if not runs:
            runs.append([row_number])
            continue
        if runs[-1][-1] == row_number - 1:
            runs[-1].append(row_number)
        else:
            runs.append([row_number])

    # Compact lists into ranges
    for i, run in enumerate(runs):
        runs[i] = f"{run[0]}-{run[-1]}" if len(run) > 1 else f"{run[0]}"
    return runs


import numpy as np


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def compact_errors_memory(
    errors: Dict[str, List], outfile: str, logger: logging.Logger = None
):
    """Reads an error file and generates a condensed version of it, saved to the outfile path.

    Args:
        errors (Dict[str, List]): map from sections (images, taxa) to list of errors, errors are dicts (JSON) or have
        a __dict__ property
        outfile (str): path to the output file
        logger (logging.RootLogger): logger
    """
    logger.info("Compacting errors")
    unique_error_list: Dict[str, List] = defaultdict(lambda: [])
    unique_warning_list: Dict[str, List] = defaultdict(lambda: [])
    for section, errors in errors.items():
        error_to_rows: Dict[str, List[int]] = defaultdict(lambda: [])
        warning_to_rows: Dict[str, List[int]] = defaultdict(lambda: [])
        warnings = []

        for e in errors:
            e = e.__dict__ if hasattr(e, "__dict__") else e
            row = e["row"]
            del e["row"]

            if e["message"].startswith("Warning:"):
                warnings.append(e)
                warning_string = json.dumps(e, cls=NpEncoder)
                warning_to_rows[warning_string].append(int(row) + 2)
            else:
                error_string = json.dumps(e, cls=NpEncoder)
                error_to_rows[error_string].append(int(row) + 2)

        for json_str, rows in warning_to_rows.items():
            json_warning = json.loads(json_str)
            json_warning["rows_range"] = ",".join(convert_list_to_runs(sorted(rows)))
            unique_warning_list[section].append(json_warning)

        for json_str, rows in error_to_rows.items():
            json_error = json.loads(json_str)
            json_error["rows_range"] = ",".join(convert_list_to_runs(sorted(rows)))
            unique_error_list[section].append(json_error)

        logger.info(
            f"{section:<6} | "
            f"warnings = {len(warnings):<5} | "
            f"unique warnings = {len(warning_to_rows):<5} | "
            f"errors = {len(errors):<5} | "
            f"unique errors = {len(error_to_rows):<5} |"
        )

    warning_output = json.dumps(unique_warning_list, indent=2)
    error_output = json.dumps(unique_error_list, indent=2)

    warning_file = f"warning_{outfile}"
    error_file = f"error_{outfile}"

    with open(warning_file, "w") as output_file:
        output_file.write(warning_output)
        logger.info(f"Wrote validation warnings to {warning_file}")

    with open(error_file, "w") as output_file:
        output_file.write(error_output)
        logger.info(f"Wrote validation errors to {error_file}")


def compact_errors(filename: str, outfile: str, logger: logging.RootLogger = None):
    """Reads an error file and generates a condensed version of it, saved to the outfile path.

    Args:
        filename (str): path to the error file
        outfile (str): path to the output file
        logger (logging.RootLogger): logger
    """
    logger.info("Compacting errors")
    with open(filename, "r") as error_file:
        errors = json.load(error_file)

    compact_errors_memory(errors, outfile, logger)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Formats error files to be more compact and user friendly."
    )
    parser.set_defaults(func=lambda x: parser.print_usage())
    parser.add_argument(
        "-e", "--error_file", required=False, help="Location of error file"
    )
    parser.add_argument(
        "-o",
        "--out_file",
        required=False,
        help="Outfile with validation errors, defaults to, 'Y-m-d-H-M-S.json'",
        default=time.strftime("%Y-%m-%d-%H-%M-%S") + ".json",
    )

    options = parser.parse_args()
    compact_errors(options.error_file, options.out_file, get_logger(f"{__file__}.log"))
