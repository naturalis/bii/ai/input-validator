import pandas


def left_of_postfix(val: str):
    return "_".join(val.split("_")[:-1])


def m():
    t_prev = pandas.read_csv(
        "/mnt/big/storage/waarneming-2023/source_data/all_images.csv"
    )
    t_prev = t_prev[t_prev.image_url.str.startswith("https://www.observation.org")] # TODO
    t_new = pandas.read_csv(
        "/mnt/big/storage/waarneming-2023-oct-arthr/source_data/images.csv"
    )
    new_postfix = "_new"
    prev_postfix = "_prev"



    # TODO: check for deleted images within observation?
    
    # check new/removed data
    # TODO: should be after ID consistency check
    removed_ids = set(t_prev.observation_id) - set(t_new.observation_id)
    removed = t_prev[t_prev.observation_id.isin(removed_ids)]
    print("|removed", len(removed))
    removed.to_csv("diff_removed.csv", index=False)
    
    #
    added_ids = set(t_new.observation_id) - set(t_prev.observation_id)
    added = t_new[t_new.observation_id.isin(added_ids)]
    print("|added", len(added))
    added.to_csv("diff_added.csv", index=False)
    
    
    
    # - consistency checks
    


    merged = pandas.merge(t_prev, t_new, on="image_id", suffixes=(prev_postfix, new_postfix))

    taxon_name_changes = None
    for column in merged.columns:
        if column == "image_id" or column.endswith(new_postfix):
            continue
        org_col = column.rsplit("_", 1)[0]
        if org_col + prev_postfix not in merged.columns:
            print("not there", org_col + prev_postfix)
            continue
        if org_col + new_postfix not in merged.columns:
            print("not there", org_col + new_postfix)
            continue
        print(org_col)
        # for _, row in merged.iterrows():
        #     if row[org_col + "_x"] != row[org_col + "_y"]:
        #         print(
        #             org_col, row["image_id"], row[org_col + "_x"], row[org_col + "_y"]
        #         )
        different = merged.apply(
            lambda x_: (x_[org_col + prev_postfix] != x_[org_col + new_postfix]) * 1, axis=1
        )
        print(org_col, different.sum() / len(merged))
        # print(different[different==1].index)

        bla: pandas.DataFrame = (
            merged.iloc[different[different==1].index, :]
            .groupby([org_col + prev_postfix, org_col + new_postfix])
            .size()
            .reset_index(name="counts")
        )
        if org_col == "taxon_full_name":
            taxon_name_changes = bla
        print(bla.sort_values("counts", ascending=False).head(25))
    #
    sufficient_num = 10
    # TODO: use obs iso im
    prev_taxon_counts = t_prev.drop_duplicates("observation_id").groupby("taxon_full_name").size().reset_index(name="counts")
    print(prev_taxon_counts[prev_taxon_counts.counts>=10].sum())
    new_taxon_counts = t_new.drop_duplicates("observation_id").groupby("taxon_full_name").size().reset_index(name="counts")
    print(new_taxon_counts[new_taxon_counts.counts >= 10].sum())
    counts_merged = pandas.merge(prev_taxon_counts, new_taxon_counts, on="taxon_full_name",
                                 suffixes=(prev_postfix, new_postfix), how="outer")
    counts_merged.fillna(0, inplace=True)
    counts_merged["sufficient_added"] = counts_merged.apply(
        lambda row_: row_["counts_new"] >= sufficient_num and row_["counts_prev"] < sufficient_num, axis=1)
    counts_merged["sufficient_removed"] = counts_merged.apply(
        lambda row_: row_["counts_prev"] >= sufficient_num and row_["counts_new"] < sufficient_num, axis=1)
    prev_name_changes = set(taxon_name_changes.taxon_full_name_prev.unique())
    new_name_changes = set(taxon_name_changes.taxon_full_name_new.unique())
    counts_merged["prev_in_label_changes"] = counts_merged["taxon_full_name"].apply(lambda x_: x_ in prev_name_changes)
    counts_merged["new_in_label_changes"] = counts_merged["taxon_full_name"].apply(lambda x_: x_ in new_name_changes)
    counts_merged.to_csv("counts_merged.csv", index=False)
    print(counts_merged[counts_merged.sufficient_added & ~counts_merged.new_in_label_changes])
    print(counts_merged[counts_merged.sufficient_removed & ~counts_merged.prev_in_label_changes])


if __name__ == "__main__":
    m()
