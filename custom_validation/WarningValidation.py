from pandas_schema.validation import InListValidation


class WarningInListValidation(InListValidation):
    def __init__(
        self,
        is_warning: bool = True,
        **kwargs,
    ):
        """
        Wrapper around InListValidation to allow warnings instead of strict errors.

        :param is_warning: If True, classify violations as warnings; otherwise, as
            errors.
        """
        self.is_warning = is_warning

        super().__init__(**kwargs)

    @property
    def message(self):
        status = "Warning:" if self.is_warning else "Error:"
        if self._custom_message:
            return f"{status} {self._custom_message}"
        else:
            return f"{status} {self.default_message}"
