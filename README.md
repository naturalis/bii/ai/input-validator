# input-validator

Tool to validate the input files for the naturalis-ai 
model building pipeline. 

## Input files
Input files specification document:  
https://docs.google.com/document/d/1iMVFLkt9P71X4Oeeik32VuEaRLAB4ifNjui5Yk6_br0

## Usage

### Installation

With python installed on your system, create and activate a virtual environment.

`python3 -m venv validator`

`source validator/bin/activate`

Then, in the environment, install the required packages using pip.

`python3 -m pip install -r requirements.txt`

Finally, use the following to start validating your files. 
`python3 validate.py [OPTIONS]`


### Options
See also: `python3 validate.py -h`

 * --images_file (-i) - location of images file
 * --taxa_file (-t) - location of taxa file
 * --outfile (-o) - specify file where validation errors are written (json)
 * --number_of_threads - number of threads should be used to process data

**Note**: Using a number larger than 1 for --number_of_threads can consume lots of memory with big files, but can speed up the script. Use with caution. 

Either taxa, images, or both files can be validated at the same time.
Note that for cross-checking the images whith the taxa file, both files must be provided.

## Output:
Validation errors are written to JSON. Example:

```
{
    "example_data/taxa_qualified_id_wrong.csv": [
        {
            "message": "Not a qualified id:",
            "value": "199",
            "row": 2,
            "column": "taxon_id_at_source"
        }
    ]
}
```

 ### Example
 ```
 python3 validate.py -t example_data/taxa.csv -i example_data/images.csv  -o errors.json --number_of_threads 2
```

## What is being validated?
 * all mandatory columns listed in the specification document are present 
 * no leading and trailing whitespaces are present

### Images file
 * column `image_url`: values are unique and not empty
 * column `observation_id`: values are valid IDs (see specs document) and are not empty
 * column `image_id`: values are unique, not empty and are valid IDs
 * column `taxon_id_at_source`: values are valid IDs and exist in taxa file (only checked if also taxa file is given)
 * column `taxon_full_name`: values not empty
 * column `sex`: no checks against controlled vocabulary, can be empty
 * column `morph`: no checks against controlled vocabulary, can be empty
 * column `morph_id`: values are valid ID or empty
 * column `location_latitude`: values (if given) are in range -90,90, no decimal comma
 * column `location_longitude`: values (if given) are in range -180,180, no decimal comma
 * column `rijkdriehoeksstelsel_x`: values have no decimal comma
 * column `rijkdriehoeksstelsel_y`: values have no decimal comma
 * column `date`: values are in format `YYYY-MM-DD`
 * column `time`: values are in format `HH:MM`

### Taxa file
 * column: `taxon_full_name`: values not empty
 * column `taxon_id_at_source`: values are unique, non empty  and valid IDs
 * column `status_at_source`: values not empty and either `accepted` or `synonym`
 * column `accepted_taxon_id_at_source`: values either valid ID or empty; if non-empty check if id exists in `taxon_id_at_source`
 * column `kingdom`: values not empty
 * columns `division`, `class`, `order`: values not empty unless kingdom is 'Plantae' or 'Fungi'
 * combination of columns `taxon_full_name` and `family` must be unique
 * columns `family`, `genus`, `specific_epithet`, `infraspecific_epithet`: no checks
 

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`
